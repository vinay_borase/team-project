import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

class PharmacyScreen extends StatefulWidget {
  const PharmacyScreen({super.key});

  @override
  State<PharmacyScreen> createState() => _PharmacyScreenState();
}

class _PharmacyScreenState extends State<PharmacyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back_ios),
          iconSize: 22,
          color: Colors.black,
        ),
        title: Text(
          "Pharmacy",
          style: GoogleFonts.inter(
            textStyle: const TextStyle(
                fontSize: 16, fontWeight: FontWeight.w500, color: Colors.black),
          ),
        ),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.shopping_cart_outlined),
            iconSize: 22,
            color: Colors.black,
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 10,
            ),
            TextField(
              decoration: InputDecoration(
                hintText: "Find a doctor",
                prefixIcon: const Icon(
                  Icons.search_off_outlined,
                  color: Colors.black,
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 135,
              width: 335,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: const Color.fromRGBO(232, 243, 241, 1),
              ),
              child: Row(
                children: [
                  Column(
                    children: [
                      Container(
                        height: 90,
                        width: 190,
                        padding: const EdgeInsets.only(top: 21, left: 25),
                        child: Text(
                          "Order quickly with Prescription",
                          style: GoogleFonts.inter(
                            textStyle: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Colors.black),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                        width: 145,
                        height: 29,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor:
                                const Color.fromARGB(25, 154, 142, 1),
                          ),
                          onPressed: () {},
                          child: const Text(
                            "Upload Prescription",
                            style: TextStyle(
                              fontSize: 10,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 140,
                    width: 140,
                    child: Image(image: AssetImage("assets/medicins/img1.png"),fit: BoxFit.fill,)),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text(
                  "Popular Product",
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Spacer(),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    "See all",
                    style: GoogleFonts.inter(
                      textStyle:
                          const TextStyle(fontSize: 12, color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 165,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                  itemCount: 3,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.all(5),
                      child: Container(
                        height: 165,
                        width: 118,
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: Colors.grey,
                          ),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.only(top: 15,left: 10),
                                height: 68,
                                width: 68,
                                child: Image(image: AssetImage("assets/medicins/img${index+1}.png")),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                "OBH Combi",
                                style: GoogleFonts.inter(
                                  textStyle: const TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black),
                                ),
                              ),
                              Text(
                                "75 ml",
                                style: GoogleFonts.inter(
                                  textStyle: const TextStyle(
                                    fontSize: 9,
                                    color: Colors.grey),
                                ),
                              ),
                              Row(
                                children: [
                                  const Icon(Icons.attach_money,
                                    size: 17,
                                  ),
                                  Text(
                                    "99.9",
                                    style: GoogleFonts.inter(
                                      textStyle: const TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black),
                                    ),
                                  ),
                                  const Spacer(),
                                  IconButton(
                                    onPressed: (){}, 
                                    icon: const Icon(Icons.add_box_rounded,
                                      color: Colors.green,
                                    )
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text(
                  "Popular Product",
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Spacer(),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    "See all",
                    style: GoogleFonts.inter(
                      textStyle:
                          const TextStyle(fontSize: 12, color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 165,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                  itemCount: 3,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.all(5),
                      child: Container(
                        height: 165,
                        width: 118,
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: Colors.grey,
                          ),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.only(top: 15,left: 10),
                                height: 68,
                                width: 68,
                                child: Image(image: AssetImage("assets/medicins/img${index+2}.png")),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                "OBH Combi",
                                style: GoogleFonts.inter(
                                  textStyle: const TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black),
                                ),
                              ),
                              Text(
                                "75 ml",
                                style: GoogleFonts.inter(
                                  textStyle: const TextStyle(
                                    fontSize: 9,
                                    color: Colors.grey),
                                ),
                              ),
                              Row(
                                children: [
                                  const Icon(Icons.attach_money,
                                    size: 17,
                                  ),
                                  Text(
                                    "99.9",
                                    style: GoogleFonts.inter(
                                      textStyle: const TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black),
                                    ),
                                  ),
                                  const Spacer(),
                                  IconButton(
                                    onPressed: (){}, 
                                    icon: const Icon(Icons.add_box_rounded,
                                      color: Colors.green,
                                    )
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
