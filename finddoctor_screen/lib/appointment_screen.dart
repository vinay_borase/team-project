import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


class AppointmentScreen extends StatefulWidget {
  const AppointmentScreen({super.key});

  @override
  State<AppointmentScreen> createState() => _AppointmentScreenState();
}

class _AppointmentScreenState extends State<AppointmentScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(229, 229, 229, 0),
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(229, 229, 229, 0),
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back_ios),
          iconSize: 22,
          color: Colors.black,
        ),
        title: Text(
          "Appointment",
          style: GoogleFonts.inter(
            textStyle: const TextStyle(
                fontSize: 16, fontWeight: FontWeight.w500, color: Colors.black),
          ),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: const EdgeInsets.all(5),
                child: Container(
                  margin: const EdgeInsets.only(left: 60),
                  height: 125,
                  width: 327,
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Colors.black45,
                    ),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Row(
                    children: [
                      Container(
                        height: 88,
                        width: 88,
                        margin: const EdgeInsets.all(15),
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: AssetImage("assets/doctor/doctor1.png"),
                                fit: BoxFit.cover)),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Dr. Marcus Horizon",
                              style: GoogleFonts.inter(
                                textStyle: const TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black),
                              ),
                            ),
                            Text(
                              "Chardiologist",
                              style: GoogleFonts.inter(
                                textStyle: const TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.grey),
                              ),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Container(
                                  color: Colors.grey,
                                  child: const Row(
                                    children: [
                                      Icon(
                                        Icons.star,
                                        color: Color.fromRGBO(25, 154, 142, 1),
                                        size: 18,
                                      ),
                                      Text(
                                        "4,7",
                                        style: TextStyle(
                                          fontSize: 12,
                                        color: Color.fromRGBO(25, 154, 142, 1),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height:  8,
                                ),
                                Row(
                                  children: [
                                    const Icon(
                                      Icons.location_on,
                                      size: 18,
                                    ),
                                    Text(
                                      "800m away",
                                      style: GoogleFonts.inter(
                                        textStyle: const TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.black),
                                      ),
                                    ),
                                  ],
                                ),
                            
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            Row(
                children: [
                  Text(
                    "Date",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Colors.black),
                    ),
                  ),
                  const Spacer(),
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      "Change",
                      style: GoogleFonts.inter(
                        textStyle:
                            const TextStyle(fontSize: 12, color: Colors.black),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    height: 36,
                    width: 36,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromRGBO(25, 154, 142, 0.2),
                    ),
                    child: IconButton(
                      onPressed: (){}, 
                      icon: const Icon(Icons.calendar_month,
                      color: Color.fromRGBO(25, 154, 142, 1),
                    ),
                    ),
                  ),
                  Text("",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: Color.fromRGBO(85, 85, 85, 1)),
                    ),
                  ),
                ],
              ),
              const Text('__________________________________________________________',
                style: TextStyle(
                  fontSize: 15,
                  color: Color.fromRGBO(232, 243, 241, 1),
                ),
              ),
              const SizedBox(
                height: 4,
              ),
              Row(
                children: [
                  Text(
                    "Reason",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Colors.black),
                    ),
                  ),
                  const Spacer(),
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      "Change",
                      style: GoogleFonts.inter(
                        textStyle:
                            const TextStyle(fontSize: 12, color: Colors.black),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    height: 36,
                    width: 36,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromRGBO(25, 154, 142, 0.2),
                    ),
                    child: IconButton(
                      onPressed: (){}, 
                      icon: const Icon(Icons.note,
                      color: Color.fromRGBO(25, 154, 142, 1),
                    ),
                    ),
                  ),
                  Text("",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: Color.fromRGBO(85, 85, 85, 1)),
                    ),
                  ),
                ],
              ),
              const Text('__________________________________________________________',
                style: TextStyle(
                  fontSize: 15,
                  color: Color.fromRGBO(232, 243, 241, 1),
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                    "Payment Detail",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Colors.black),
                    ),
                  ),
            const SizedBox(
              height: 8,
            ),
            Row(
                children: [
                  Text(
                    "Consultation",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: Color.fromRGBO(161, 168, 176, 1)),
                    ),
                  ),
                  const Spacer(),
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      "60.00",
                      style: GoogleFonts.inter(
                        textStyle:
                            const TextStyle(fontSize: 14,
                            fontWeight: FontWeight.w600, 
                            color: Color.fromRGBO(16, 22, 35, 1)),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    "Admin Fee",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: Color.fromRGBO(161, 168, 176, 1)),
                    ),
                  ),
                  const Spacer(),
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      "01.00",
                      style: GoogleFonts.inter(
                        textStyle:
                            const TextStyle(fontSize: 14,
                            fontWeight: FontWeight.w600, 
                            color: Color.fromRGBO(16, 22, 35, 1)),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    "Additional Discount",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: Color.fromRGBO(161, 168, 176, 1)),
                    ),
                  ),
                  const Spacer(),
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      "-",
                      style: GoogleFonts.inter(
                        textStyle:
                            const TextStyle(fontSize: 14,
                            fontWeight: FontWeight.w600, 
                            color: Color.fromRGBO(16, 22, 35, 1)),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    "Total",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    ),
                  ),
                  const Spacer(),
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      "61.00",
                      style: GoogleFonts.inter(
                        textStyle:
                            const TextStyle(fontSize: 14,
                            fontWeight: FontWeight.w600, 
                            color: Color.fromRGBO(25, 154, 142, 1)),
                      ),
                    ),
                  ),
                ],
              ),
              const Text('__________________________________________________________',
                style: TextStyle(
                  fontSize: 15,
                  color: Color.fromRGBO(232, 243, 241, 1),
                ),
              ),
              Text(
                    "Payment Method",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Colors.black),
                    ),
                  ),
            const SizedBox(
              height: 8,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                decoration: InputDecoration(
                  prefixIcon: Padding(
                    padding: const EdgeInsets.only(top: 5,left: 20),
                    child: Text("VISA",
                      style: GoogleFonts.inter(
                          textStyle: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Color.fromRGBO(26, 31, 113, 1)),
                        ),
                    ),
                  ),
                  suffix: Padding(
                    padding: const EdgeInsets.only(top: 5,left: 20),
                    child: Text("Change",
                      style: GoogleFonts.inter(
                          textStyle: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: Color.fromRGBO(173, 173, 173, 1)),
                        ),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Column(
                  children: [
                    Text("Total",
                      style: GoogleFonts.inter(
                          textStyle: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: Color.fromRGBO(161, 168, 176, 1)),
                        ),
                    ),
                    Text("61.00",
                      style: GoogleFonts.inter(
                          textStyle: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w800,
                              color: Color.fromRGBO(16, 22, 35, 1)),
                        ),
                    ),
                  ],
                ),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: SizedBox(
                    height: 50,
                    width: 192,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color.fromRGBO(25, 154, 142, 1),
                      ),
                      onPressed: (){}, 
                      child: Text("Booking",
                        style: GoogleFonts.inter(
                              textStyle: const TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white),
                            ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}