import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


class DrugeDetailScreen extends StatefulWidget {
  const DrugeDetailScreen({super.key});

  @override
  State<DrugeDetailScreen> createState() => _DrugeDetailScreenState();
}

class _DrugeDetailScreenState extends State<DrugeDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(229, 229, 229, 0),
      appBar: AppBar(
      backgroundColor: const Color.fromRGBO(229, 229, 229, 0),
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back_ios),
          iconSize: 22,
          color: Colors.black,
        ),
        title: Text(
          "Drugs Detail",
          style: GoogleFonts.inter(
            textStyle: const TextStyle(
                fontSize: 16, fontWeight: FontWeight.w500, color: Colors.black),
          ),
        ),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.shopping_cart_outlined),
            iconSize: 22,
            color: Colors.black,
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 163,
              width: 163,
              margin: const EdgeInsets.symmetric(horizontal: 150,vertical: 10),
              child: const Image(image: AssetImage('assets/medicins/img5.png')),
            ),
            Text(
              "OBH Combi",
              style: GoogleFonts.inter(
                textStyle: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  color: Colors.black),
              ),
            ),
            Row(
              children: [
                Text("75 ml",
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 16,
                        color: Colors.grey),
                  ),
                ),
                const Spacer(),
                IconButton(
                  onPressed: (){}, 
                  icon: const Icon(Icons.favorite_border_outlined)
                ),
              ],
            ),
            const Row(
                  children: [
                    Icon(
                                      Icons.star,
                                      color: Color.fromRGBO(25, 154, 142, 1),
                                      size: 14,
                                    ),
                  Icon(
                                      Icons.star,
                                      color: Color.fromRGBO(25, 154, 142, 1),
                                      size: 14,
                                    ),
                    Icon(
                                      Icons.star,
                                      color: Color.fromRGBO(25, 154, 142, 1),
                                      size: 14,
                                    ),
                  Icon(
                                      Icons.star,
                                      color: Color.fromRGBO(25, 154, 142, 1),
                                      size: 14,
                                    ),
                    Text(
                                      "4.0",
                                      style: TextStyle(
                                        fontSize: 14,
                                      color: Color.fromRGBO(25, 154, 142, 1),
                                      ),
                                    )                
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
            Row(
              children: [
                IconButton(
                  onPressed: (){},
                  icon: const Icon(Icons.horizontal_rule,
                    size: 32,
                  )
                ),
                const SizedBox(
                  width: 8,
                ),
                Text("1",
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 24,
                        color: Colors.black),
                  ),
                ),
                const SizedBox(
                  width:  8,
                ),
                IconButton(
                  onPressed: (){}, 
                  icon: const Icon(Icons.add_box,
                  size: 32,
                    color: Color.fromRGBO(25, 154, 142, 1),
                  )
                ),
                const Spacer(),
                Text('9.99',
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 26,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            Text(
                    "Description",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Colors.black),
                    ),
                  ),
            Text(
                    "OBH COMBI  is a cough medicine containing, Paracetamol, Ephedrine HCl, and Chlorphenamine maleate which is used to relieve coughs accompanied by flu symptoms such as fever, headache, and sneezing.",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          color: Color.fromRGBO(173, 173, 173, 1)),
                    ),
                  ),
            const Spacer(),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                children: [
                  Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: const Color.fromRGBO(25, 154, 142, 0.4),
                    ),
                    child: const Icon(Icons.shopping_cart_outlined,
                      color: Color.fromRGBO(25, 154, 142, 0.7),
                    ),
                  ),
                  const Spacer(),
                  Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: SizedBox(
                      height: 50,
                      width: 192,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color.fromRGBO(25, 154, 142, 1),
                        ),
                        onPressed: (){}, 
                        child: Text("Booking",
                          style: GoogleFonts.inter(
                                textStyle: const TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white),
                              ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}